from django.contrib import admin
from .models import Location, Bin

@admin.register(Bin)
class BinsAdmin(admin.ModelAdmin):
    pass

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass
