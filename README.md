# Wardrobify

Team:

* Marvin Xia - Hats Microservice
* Taylor Harris - Shoes Microservice

## Design

## Shoes microservice
started out by creating two models, one for my shoes and one as a value object for the bin that i would be polling in.
created the views to be able to list all the shoes, make new shoes and another view that will delete a single shoe.
once these views worked in insomnia, i worked on my poller to be able to create real data.
once all my data worked and was talking to the wardrobe api, i moved onto the front end.
first thing i did was create a shoelist.js to be able to list out all my shoes. i chose a card style for the display becuase i thought it would give the user a nice full view of all their shoes.
i then moved onto the shoe form. this had to both get the bin data and the shoe data properly to know what to create and where to send it.
once that worked i finished up the delete function back in the shoelist.js


## Hats microservice
Created Hat and LocationVO models. The Hat model takes in information regarding style, color fabric, picture and shares a foreign key relationship with LocationsVO. The LocationsVO model takes in closet name, section and shelf number info.

An api is used to create the hats requested and poll an id for location.

On the front end view hats can be shown as a list and a sepearte form page can be loaded up through a create button. Hats can be deleted as well through its own sepearte delete button function.
