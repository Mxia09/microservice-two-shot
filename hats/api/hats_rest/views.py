from django.shortcuts import render
from django.http import JsonResponse
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder 

# Create your views here.
class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "color",
        "style",
        "picture",
        "id"
    ]
    
class HatsDetailsEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "color",
        "style",
        "picture",
        "id"
    ]

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )

    else:
        content = json.loads(request.body)
    try:
        location_href = content.get("location") 
        location = LocationVO.objects.get(id=location_href)
        # location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location
    except LocationVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Hat id"},
            status=400,
        )

    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatsDetailsEncoder,
        safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatsDetailsEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                {"message": "Hat deleted successfully"},
                status=204
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response

