from django.db import models
from django.urls import reverse

class binVO(models.Model):
    closet_name = models.CharField(max_length=200)
    # bin_size = models.PositiveSmallIntegerField(default='0')
    # bin_number = models.PositiveSmallIntegerField(default='0')
    import_href = models.CharField(max_length=200, unique=True, blank=True)




class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200, null=True, blank=True)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    photo = models.URLField()
    bin = models.ForeignKey(
        binVO,
        related_name="bin",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.model_name
                # f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    def get_api_url(self):
        return reverse("show_shoes", kwargs={"pk": self.pk})
