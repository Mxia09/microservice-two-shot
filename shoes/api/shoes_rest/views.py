from django.shortcuts import render
from .models import Shoes, binVO
import json
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

class BinVOEncoder(ModelEncoder):
    model = binVO
    properties = ["closet_name",
                "import_href",]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "photo",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_shoes(request):

    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            bin_id = content["bin"]
            # print(bin_id)
            bin = binVO.objects.get(id=bin_id)
            content["bin"] = bin
        except binVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin ID"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )

    # elif request.method == "DELETE":
    #     try:
    #         bin_id = content["bin"]
    #         shoes = binVO.objects.get(id=bin_id)
    #         content.delete()
    #         return JsonResponse(
    #             conference,
    #             encoder=ConferenceDetailEncoder,
    #             safe=False,
    #         )
    #     except binVO.DoesNotExist:
    #         return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "DELETE"])
def show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
