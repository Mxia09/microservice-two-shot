import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList.js'
import ShoeForm from './ShoeForm.js'
import HatsList from './HatsList';
import HatsForm  from './HatsForm'

// creating setShoes to pass into the the shoe form via the browser router
function App() {

  const [ shoes, setShoes ] = useState([]);

  async function getShoes() {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  }

  useEffect(() => {
    setShoes();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/" element={<ShoeList />} />
          <Route path="shoes/create/" element={<ShoeForm setShoes={setShoes}/>} />
          <Route path="hats/" element={<HatsList />} />
          <Route path="hats/create" element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
