import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom';


// function shoeCard(shoe) {
//     return (
//         <div className='card my-3 p-3 rounded'>
//                 <h3>{shoe.model_name}</h3>
//         </div>
//         )
//     }


function ShoeList() {

    const [shoes, setShoes] = useState([])

    async function fetchShoes(){
        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes)
            // console.log(setShoes)
        }
    }
    useEffect(() => {
        fetchShoes();
        // console.log(setShoes)
        }, []);

    const deleteShoe = async (id) => {
        // console.log(id)
        const shoeUrl = `http://localhost:8080${id}`
        const response = await fetch(shoeUrl, {method: "DELETE"})
        // console.log(id)
        if (response.ok) {
            setShoes(shoes.filter((shoe) => shoe.id !== id));
            console.log("NUKED")
            }
        }


    return(
        <div className="container">
            <h2>All Shoes</h2>
            <Link to="/shoes/create/" className='btn btn-primary'>Create New Shoe</Link>

            <div className='row'>
            {shoes.map((shoe, pk) => (
                <div key={shoe.id} className='col'>
                        {/* { console.log(shoe)} */}
                    <div className='card my-3 p-3 rounded'>
                        <img className='img-fluid' src={shoe.photo}/>

                        <h5 className="card-title ">{shoe.model_name}</h5>

                        <div className='card-body'>
                            <p>
                                <strong>Manufacturer:</strong> {shoe.manufacturer}
                            </p>
                            <p>
                                <strong>Color:</strong> {shoe.color}
                            </p>
                            <p>
                                <strong>Bin:</strong> {shoe.bin.closet_name}
                            </p>
                            <div className='d-grid'>
                            <button onClick={() => deleteShoe(shoe.href)} className="btn btn-primary" >Delete</button>

                            </div>
                        </div>

                    </div>

                </div>

            ))}
        </div>
    </div>
    )
}

export default ShoeList
