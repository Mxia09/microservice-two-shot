import React, {useState, useEffect} from 'react'




// in this form you can create a new Shoe. this created useStates, grabs all the bins, has functions to handle the submit
// and then a .map function for the <select> tag that will create a new option for each bin there is and link it
// the that bin in the database

function ShoeForm({ setShoes }) {

    const [manufacturer, setManufacturer] = useState('')
    const [model_name, setModelName] = useState('')
    const [color, setColor] = useState('')
    const [photo, setPhoto] = useState('')
    const [bin, setBin] = useState('')
    const [bins, setBins] = useState([])

    useEffect(() => {
        async function getBins() {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);

            if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
            console.log(data.bins)
            }
        }
        getBins();
        }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
        manufacturer,
        model_name,
        color,
        photo,
        bin: bin,
    };


    const locationUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);
        setShoes();

        setManufacturer('');
        setModelName('');
        setColor('');
        setPhoto('');
        setBin('');
        }
    }


    function handleManufacturer(event) {
        const value = event.target.value;
        setManufacturer(value);
        }

    function handleModelName(event) {
        const value = event.target.value;
        setModelName(value);
    }

    function handleColor(event) {
        const value = event.target.value;
        setColor(value);
    }

    function handlePhoto(event) {
        const value = event.target.value;
        setPhoto(value);
    }

    function handleBins(event) {
        const value = event.target.value;
        setBin(value);
    }

    return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create New Shoes</h1>
                <form onSubmit={handleSubmit} id="create-shoes-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleManufacturer} value={manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">manufacturer</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input onChange={handleModelName} value={model_name} placeholder="model_name" required type="text" name="model_name" id="starts" className="form-control" />
                    <label htmlFor="model_name">model name</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input onChange={handleColor} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">color</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input onChange={handlePhoto} value={photo} placeholder="photo" required type="text" name="photo" id="photo" className="form-control" />
                    <label htmlFor="photo">photo</label>
                    </div>

                    <div className="mb-3">
                    <select onChange={handleBins} value={bin} id="bin" multiple={false} className="form-select">
                    <option value="">Choose a bin</option>

                    {bins.map(bin => {
                        return (
                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                        )
                    })}

                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        );

    }

export default ShoeForm

